// swift-tools-version:5.4
import PackageDescription
let package = Package(
    name: "Suitest",
    platforms: [
        .macOS(.v10_12), .tvOS(.v11)
    ],
    products: [
        .library(
            name: "Suitest",
            targets: ["Suitest", "Suitest-tvOS"]
        )
    ],
    dependencies: [
        .package(name: "CryptoSwift", url: "https://github.com/krzyzanowskim/CryptoSwift", from: "1.4.1"),
        .package(name: "Starscream", url: "https://github.com/daltoniam/Starscream", from: "3.0.6"),
        .package(name: "SwiftyXML", url: "https://github.com/chenyunguiMilook/SwiftyXML", from: "3.1.0"),
        .package(name: "Fuzi", url: "https://github.com/cezheng/Fuzi", from: "3.1.1"),
        .package(name: "Reachability", url: "https://github.com/ashleymills/Reachability.swift", from: "4.3.1")
        ],
    targets: [
        .binaryTarget(
            name: "Suitest-tvOS",
            path: "Suitest.xcframework"
        ),
        .target(name: "Suitest",
                dependencies:[
                    "CryptoSwift", "Fuzi", "Starscream", "Reachability", "SwiftyXML"
                ],
        path: "Suitest-dependencies")
        ]
)
